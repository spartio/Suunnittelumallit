
package S18prototype;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author spart
 */
public class MinuuttiViisari implements Viisari, Runnable {
    private int minuutti;

    public MinuuttiViisari(int minuutti) {
        this.minuutti = minuutti;
    }
    

    public int getAika() {
        return minuutti;
    }

    public void setMinuutti(int minuutti) {
        this.minuutti = minuutti;
    }
    
    @Override
    public Viisari kloonaa() {
       throw new UnsupportedOperationException("Not supported yet.");     
    }

    @Override
    public void run() {
        while(true) {
            minuutti++;
            try {
                Thread.sleep(60000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

}
