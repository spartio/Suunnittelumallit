
package S18prototype;

/**
 * @author spart
 */
public class Main {
    public static void main(String[] args) {
        Viisari tunti = new TuntiViisari(14);
        Viisari minuutti = new TuntiViisari(1);
        Viisari sekuntti = new TuntiViisari(5);
        
        Kello kello = new Kello(tunti, minuutti, sekuntti);
        
        new Thread(tunti).start();
        new Thread(minuutti).start();
        new Thread(sekuntti).start();
    
        while (true) 
            kello.tulostaAika();
    }
}
