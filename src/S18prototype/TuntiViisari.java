
package S18prototype;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author spart
 */
public class TuntiViisari implements Viisari, Runnable {
    private int tunti;

    public TuntiViisari(int tunti) {
        this.tunti = tunti;
    }

    public int getAika() {
        return tunti;
    }

    public void setTunti(int tunti) {
        this.tunti = tunti;
    }
    
    @Override
    public Viisari kloonaa() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void run() {
        while(true) {
        tunti++;
            try {
                Thread.sleep(3600000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
