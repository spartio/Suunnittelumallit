
package S18prototype;

/**
 * @author spart
 */
public class Kello implements Cloneable {
    private Viisari tunti, minuutti, sekuntti;

    public Kello(Viisari tunti, Viisari minuutti, Viisari sekuntti) {
        this.tunti = tunti;
        this.minuutti = minuutti;
        this.sekuntti = sekuntti;
    }
    
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) { 
            return null;
        }
    }
    
    public void tulostaAika() {
        System.out.println(tunti.getAika() + ":" + minuutti.getAika() + ":" +sekuntti.getAika());
    }

}
