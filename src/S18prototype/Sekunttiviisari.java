
package S18prototype;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author spart
 */
public class Sekunttiviisari implements Viisari, Runnable {
    private int sekuntti;

    public Sekunttiviisari(int sekuntti) {
        this.sekuntti = sekuntti;
    }

    public int getAika() {
        return sekuntti;
    }

    public void setSekuntti(int sekuntti) {
        this.sekuntti = sekuntti;
    }
    @Override
    public Viisari kloonaa() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void run() {
        while(true) {
            sekuntti++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

}
