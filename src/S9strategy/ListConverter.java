package S9strategy;

import java.util.List;

/**
 *
 * @author spart
 */
public interface ListConverter {
    public String listToString(List<String> l);
}
