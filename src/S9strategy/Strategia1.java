
package S9strategy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author spart
 */
public class Strategia1 implements ListConverter {
    Iterator<String> iterator;

    @Override
    public String listToString(List<String> l) {
        String list="";
        iterator = l.iterator();
        while (iterator.hasNext()) {
            list+=iterator.next();
            list+="\n";
        }
        
        return list;
    }
}
