
package S9strategy;

import java.util.List;

/**
 * lista taulukoksi ennen läpikäyntiä, joka toteutetaan for silmukassa
 */
public class Strategia2 implements ListConverter {

    @Override
    public String listToString(List<String> l) {
        String list="";
        String[] array = new String[l.size()];
        int i=0;
        for (String s : l) {
            array[i] = s;
            i++;
        }

        for (int j=0; j<array.length; j++) {
            list+=array[j];
            if (j%2==1)
                list+="\n";
        }

        return list;
    }

}
