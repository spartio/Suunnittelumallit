
package S9strategy;

import java.util.List;

/**
 * lista läpikäynti for silmukassa käyttäen List rajapinnan get metodia
 */
public class Strategia3 implements ListConverter {

    @Override
    public String listToString(List l) {
        String list="";
        
        for (int i=1; i<l.size()+1; i++) {
            list+=l.get(i-1);
            if (i%3==0)
                list+="\n";
        }
        
        return list;
    }

}
