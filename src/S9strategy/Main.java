
package S9strategy;

import java.util.ArrayList;
import java.util.List;

/**
 * @author spart
 */
public class Main {
    public static void main(String[] args) {
        ListConverter converter = new Strategia1();

        List<String> list = new ArrayList();
        
        String a, b, c, d, e, f, g, h;
        
        a="a"; b="b"; c="c"; d="d"; e="e"; f="f"; g="g"; h="h";
        
        list.add(a);
        list.add(b);
        list.add(c);
        list.add(d);
        list.add(e);
        list.add(f);
        list.add(g);
        list.add(h);
        
        System.out.println(converter.listToString(list));
        
        converter = new Strategia2();
        System.out.println(converter.listToString(list));
        
        converter = new Strategia3();
        System.out.println(converter.listToString(list));
    }
}
