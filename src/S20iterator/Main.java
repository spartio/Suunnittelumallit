
package S20iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author spart
 */
public class Main {
    static List<String> numeroLista;
    static Iterator<String> listaIterator1, listaIterator2;
    
    
    public static void main(String[] args) {
        numeroLista = new ArrayList<String>();
        listaIterator1 = numeroLista.iterator();
        listaIterator2 = numeroLista.iterator();
        
        numeroLista.add("1");
        numeroLista.add("2");
        numeroLista.add("3");
        numeroLista.add("4");
        numeroLista.add("5");
        numeroLista.add("6");
        numeroLista.add("7");
        numeroLista.add("8");

        //eriIteraattorit();
        //samaIteraattori();
        muutoksiaListassa();
    }
    
    //molemmat iteraattorit tulostavat kaikki listan alkiot
    public static void eriIteraattorit() {
        Säie s1 = new Säie(numeroLista, listaIterator1, "säie1:");
        Säie s2 = new Säie(numeroLista, listaIterator2, "säie2:");
        
        new Thread(s1).start();
        new Thread(s2).start();
    }

    public static void samaIteraattori() {
        Säie s1 = new Säie(numeroLista, listaIterator1, "säie1:");
        Säie s2 = new Säie(numeroLista, listaIterator1, "säie2:");
        
        new Thread(s1).start();
        new Thread(s2).start();
    }
    
    public static void muutoksiaListassa() {
        Säie s1 = new Säie(numeroLista, listaIterator1, "säie1:");
        Säie s2 = new Säie(numeroLista, listaIterator1, "säie2:");
        
        new Thread(s1).start();
        new Thread(s2).start();
        
        numeroLista.add("9");
        numeroLista.add("10");
    }
}
