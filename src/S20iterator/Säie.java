
package S20iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author spart
 */
public class Säie implements Runnable {
    List<String> numeroLista = new ArrayList<String>();
    Iterator<String> listaIterator = numeroLista.iterator();
    String nimi;
    
    public Säie(List<String> list, Iterator<String> iterator, String nimi) {
        this.numeroLista=list;
        this.listaIterator=iterator;
        this.nimi=nimi;
    }
    
    @Override
    public void run() {
        Iterator<String> listaIterator = numeroLista.iterator();
        while (listaIterator.hasNext()) {
            System.out.println(nimi+listaIterator.next());
        }
    }

}
