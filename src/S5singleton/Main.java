
package S5singleton;

import java.io.IOException;
import java.util.Properties;

/**
 * @author spart
 */
public class Main {
    public static void main(String[] args) {
        AbstractTehdas tehdas = TehdasKoira.getInstance();
        AbstractJalat jalat = tehdas.luoJalat();
        AbstractKarvat karvat = tehdas.luoKarvat();
        
        System.out.println("eläin:" + tehdas.getNimi());
        System.out.println("jalat:" + jalat);
        System.out.println("karvaa:" + karvat);


    }
}
