
package S5singleton;

/**
 * @author spart
 */
public class TehdasKoira implements AbstractTehdas {
    private static TehdasKoira instance=null;
    private String nimi = "koira";
    private TehdasKoira(){}
    
    public static TehdasKoira getInstance() {
        if (instance==null)
            instance=new TehdasKoira();
        return instance;
    }
    
    @Override
    public AbstractJalat luoJalat() {
        return new JalatKoira();
    }

    @Override
    public AbstractKarvat luoKarvat() {
        return new KarvatKoira();
    }

    public String getNimi() {
        return nimi;
    }
}
