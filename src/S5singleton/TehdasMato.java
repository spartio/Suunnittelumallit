
package S5singleton;

/**
 * @author spart
 */
public class TehdasMato implements AbstractTehdas{
    private static TehdasMato instance=null;
    private String nimi = "kastemato";

    private TehdasMato(){}
    
    public static TehdasMato getInstance() {
        if (instance==null)
            instance=new TehdasMato();
        return instance;
    }

    @Override
    public AbstractJalat luoJalat() {
        return new JalatMato();
    }

    @Override
    public AbstractKarvat luoKarvat() {
        return new KarvatMato();
    }
    
    public String getNimi() {
        return nimi;
    }
}
