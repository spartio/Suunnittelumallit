package S5singleton;

/**
 *
 * @author spart
 */
public interface AbstractTehdas {
    public AbstractJalat luoJalat();
    public AbstractKarvat luoKarvat();
    public String getNimi();
}
