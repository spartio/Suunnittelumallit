
package S8templatemethod;

import java.util.ArrayList;
import java.util.List;

/**
 * @author spart
 */
public class Main {
    public static void main(String[] args) {
        ProPilkki pilkki = new ProPilkki();
        List<Pelaaja> pelaajat = new ArrayList();
        
        Pelaaja pertti = new Pertti();
        Pelaaja jarmo = new Jarmo();
        pelaajat.add(pertti);
        pelaajat.add(jarmo);
        
        pilkki.pelaaYksiPeli(pelaajat);
    }
}
