
package S8templatemethod;

import java.util.ArrayList;
import java.util.List;

/**
 * @author spart
 */
public class Pertti implements Pelaaja {
    List<Kala> reppu = new ArrayList();
    private String nimi="Pertti";
    
    public void laitaReppuun(Kala k) {
        reppu.add(k);
    }
    
    public void tulostaReppu() {
        int summa=0;
        for (Kala k : reppu) {
            summa+=k.getPisteet();
        }
        System.out.println("Repun summa: " +summa);
        System.out.print("Repun sisältö: ");
        System.out.println(reppu.toString());
    }

    public void tulostaNimi() {
        System.out.println(nimi);
    }
}
