
package S8templatemethod;

/**
 * @author spart
 */
public class Silakka implements Kala {
    private int pisteet=10;
    private String nimi="Silakka";

    public int getPisteet() {
        return pisteet;
    }
    
     public String toString() {
        return nimi;
    }
}
