
package S8templatemethod;

/**
 * @author spart
 */
public class Ahven implements Kala {
    private int pisteet=20;
    private String nimi="Ahven";
    
    public int getPisteet() {
        return pisteet;
    }

    public String toString() {
        return nimi;
    }
    
}
