
package S8templatemethod;

/**
 * @author spart
 */
public class Kuha implements Kala {
    private int pisteet=100;
    private String nimi = "Kuha";

    public int getPisteet() {
        return pisteet;
    }

    public String toString() {
        return nimi;
    }    
}
