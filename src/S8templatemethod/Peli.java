
package S8templatemethod;

import java.util.List;

/**
 * @author spart
 */
public abstract class Peli {
    abstract void initialisoi();
    abstract void teeSiirto(Pelaaja p);
    abstract boolean pelinLoppu();
    abstract void printVoittaja();
    
    public final void pelaaYksiPeli(List<Pelaaja> pelaajaLista) {
        initialisoi();
        while (pelinLoppu()) {
            for (Pelaaja p : pelaajaLista) {
                teeSiirto(p);
            }
        }
        printVoittaja();
    }
}
