package S8templatemethod;

/**
 *
 * @author spart
 */
public interface Pelaaja {
    public void laitaReppuun(Kala k);
    public void tulostaReppu();
    public void tulostaNimi();
}
