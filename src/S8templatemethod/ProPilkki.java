
package S8templatemethod;

import java.util.Random;

/**
 * @author spart
 */
public class ProPilkki extends Peli {
    Kala ahven = new Ahven();
    Kala silakka = new Silakka();
    Kala kuha = new Kuha();
    Pelaaja voittaja;
    Random random = new Random();
    private boolean pelaa;
    
    @Override
    void initialisoi() {
        pelaa = true; //true=peli kesken, false=lopeta peli
    }

    @Override
    void teeSiirto(Pelaaja pelaaja) {
        int arvo = random.nextInt(11 - 1) + 1;
        if (arvo==10) {
            pelaaja.laitaReppuun(kuha);
            voittaja=pelaaja;
            pelaa=false;
        }
        else if (arvo>5)
            pelaaja.laitaReppuun(ahven);
        else
            pelaaja.laitaReppuun(silakka);
    }

    @Override
    boolean pelinLoppu() {
        return pelaa;
    }

    @Override
    void printVoittaja() {
        System.out.print("Voittaja: ");
        voittaja.tulostaNimi();
        voittaja.tulostaReppu();
    }
}
