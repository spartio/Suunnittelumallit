
package S21facade;

/**
 * @author spart
 */
public class RawMaterialGoods implements Goods {
    String name="raw materials";
    
    @Override
    public String toString() {
        return name;
    }
}
