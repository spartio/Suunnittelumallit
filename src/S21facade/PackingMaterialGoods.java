
package S21facade;

/**
 * @author spart
 */
public class PackingMaterialGoods implements Goods {
    String name="packing materials";
    
    @Override
    public String toString() {
        return name;
    }
    
}
