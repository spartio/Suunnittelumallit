
package S21facade;

/**
 * @author spart
 */
public class FinishedGoods implements Goods {
    String name="finished goods";
    
    @Override
    public String toString() {
        return name;
    }
}
