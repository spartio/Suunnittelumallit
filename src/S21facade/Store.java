package S21facade;

/**
 *
 * @author spart
 */
public interface Store {
    public Goods getGoods();
}
