
package S21facade;

/**
 * @author spart
 */
public class RawMaterialStore implements Store {

 	public Goods getGoods() {
            RawMaterialGoods rawGoods = new RawMaterialGoods();
            return rawGoods;
        }       
}
