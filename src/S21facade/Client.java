
package S21facade;

/**
 * @author spart
 */
public class Client {

    public static void main(String[] args) {
        StoreKeeper keeper = new StoreKeeper();
        
        RawMaterialGoods rawMaterialGoods = keeper.getRawMaterialGoods();
        FinishedGoods finishedGoods = keeper.getFinishedGoods();
        
        System.out.println(rawMaterialGoods);
        System.out.println(finishedGoods);
    }
}
