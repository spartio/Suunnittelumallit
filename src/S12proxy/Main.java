
package S12proxy;

import java.util.ArrayList;
import java.util.List;

/**
 * @author spart
 */
public class Main {
   public static void main(final String[] arguments) {
        final Image image1 = new ProxyImage("HiRes_10MB_Photo1");
        final Image image2 = new ProxyImage("HiRes_10MB_Photo2");

        List<Image> photofolder = new ArrayList();
        photofolder.add(image1);
        photofolder.add(image2);
        
        for (Image i : photofolder) {
            i.printImageName();
        }
        
        image1.displayImage(); // loading necessary
        image1.displayImage(); // loading unnecessary
        image2.displayImage(); // loading necessary
        image2.displayImage(); // loading unnecessary
        image1.displayImage(); // loading unnecessary
    }
}
