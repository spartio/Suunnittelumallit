package S12proxy;

/**
 *
 * @author spart
 */
public interface Image {
    public void displayImage();
    public void printImageName();
}
