
package S22command;

/**
 * @author spart
 */
public class Nappi {
    Komento komento;
    
    public Nappi(Komento cmd){
        komento = cmd;
    }
    
    public void push() {
        komento.execute();
    }
}
