
package S22command;

/**
 * @author spart
 */
public class TestaaValkoKangas {
    public static void main(String[] args){

    ValkoKangas kangas = new ValkoKangas();

    Komento ylös = new KangasYlös(kangas);
    Komento alas = new KangasAlas(kangas);

    Nappi nosta = new Nappi(ylös);
    Nappi laske = new Nappi(alas);

    nosta.push();
    laske.push();
    }
}
