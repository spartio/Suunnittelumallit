
package S22command;

/**
 * @author spart
 */
public class ValkoKangas {
    
    public void nosta() {
        System.out.println("Nostetaan kangasta...");
    }
    
    public void laske() {
        System.out.println("Lasketaan kangasta...");
    }
}
