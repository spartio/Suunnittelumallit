
package S22command;

/**
 * @author spart
 */

public class KangasAlas implements Komento {

    private ValkoKangas kangas;

    public KangasAlas(ValkoKangas kangas) {
        this.kangas = kangas;
    }

    @Override // Komento
    public void execute() {
        kangas.laske();
    }
}
