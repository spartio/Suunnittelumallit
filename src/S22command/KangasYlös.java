
package S22command;

/**
 * @author spart
 */
public class KangasYlös implements Komento {
    private ValkoKangas kangas;
    
    public KangasYlös(ValkoKangas kangas) {
        this.kangas = kangas;
    }

    @Override // Komento
    public void execute() {
        kangas.nosta();
    }
}
