
package S3composite;

import java.util.ArrayList;
import java.util.List;

/**
 * @author spart
 */
public class Verkkokortti implements Osa {
    private final double OMAHINTA = 1.2;
    private double summa;
    
    List<Osa> verkkolista = new ArrayList();
   
    @Override
    public double laskeHinta() {
        for (Osa mp : verkkolista) {
            summa += mp.laskeHinta();
        }
        summa += OMAHINTA;
        
        System.out.println("verkkokortti:" + summa);
        return summa;
    }

    @Override
    public void lisääOsa(Osa osa, int määrä) {
        for (int i=0; i<määrä; i++) {
            verkkolista.add(osa);
        }
    }

}
