
package S3composite;

import java.util.ArrayList;
import java.util.List;

/**
 * @author spart
 */
public class Prosessori implements Osa {

    private final double OMAHINTA = 3.7;
    private double summa;
    
    List<Osa> prossulista = new ArrayList();
    
    @Override
    public double laskeHinta() {
        for (Osa mp : prossulista) {
            summa += mp.laskeHinta();
        }
        
        summa += OMAHINTA;
        System.out.println("prossu:" + summa);
        return summa;
    }

    @Override
    public void lisääOsa(Osa osa, int määrä) {
        for (int i=0; i<määrä; i++) {
            prossulista.add(osa);
        }
    }

}
