
package S3composite;

/**
 * mp = muistipiiri
 * 
 *                 kotelo  
 *                  /    \
 *                 /      \
 *             emolevy    prossu
 *             /  |  \      /  \
 *            /   |   \    mp  mp
 *           /    mp   \        
 *          /           \
 *      näyttäri    verkkokortti
 *       / \             / \
 *      /   \           /   \
 *     mp    mp        mp    mp
 * 
 * HINNAT:
 * mp=1.5
 * emo=4.3
 * verkko=1.2
 * näyttäri=2.2
 * kotelo=6.8
 * prossu=3.7
 * 
 */
public class Main {
    public static void main(String[] args) {
        
        Osa mp = new Muistipiiri();

        Osa näyttäri = new Näytönohjain();
        näyttäri.lisääOsa(mp,2);
        
        Osa verkko = new Verkkokortti();
        verkko.lisääOsa(mp,2);
        
        Osa emo = new Emolevy();
        emo.lisääOsa(näyttäri,1);
        emo.lisääOsa(verkko,1);
        emo.lisääOsa(mp,1);
        
        Osa prossu = new Prosessori();
        prossu.lisääOsa(mp, 2);
        
        Osa kotelo = new Kotelo();
        kotelo.lisääOsa(prossu, 1);
        kotelo.lisääOsa(emo, 1);

        kotelo.laskeHinta();
    }
}
