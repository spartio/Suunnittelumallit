
package S3composite;

import java.util.ArrayList;
import java.util.List;

/**
 * @author spart
 */
public class Näytönohjain implements Osa {
    final double OMAHINTA = 2.2;
    private double summa;
    
    List<Osa> näyttärilista = new ArrayList();
    
    @Override
    public double laskeHinta() {
        for (Osa mp : näyttärilista) {
            summa += mp.laskeHinta();
        }
        summa += OMAHINTA;
        System.out.println("näyttäri:" + summa);
        return summa;
    }

    
    @Override
    public void lisääOsa(Osa osa, int määrä) {
        for (int i=0; i<määrä; i++) {
            näyttärilista.add(osa);
       }
    }
}
