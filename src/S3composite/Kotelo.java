
package S3composite;

import java.util.ArrayList;
import java.util.List;

/**
 * @author spart
 */
public class Kotelo implements Osa {

    private final double OMAHINTA = 6.8;
    private double summa;
    
    List<Osa> kotelolista = new ArrayList();
    
    @Override
    public double laskeHinta() {
        for (Osa osa : kotelolista) {
            summa += osa.laskeHinta();
        }
        summa+=OMAHINTA;
       
        System.out.println("kotelo:" + summa);
        return summa;
    }

    @Override
    public void lisääOsa(Osa osa, int määrä) {
        for (int i=0; i<määrä; i++) {
            kotelolista.add(osa);
        }
    }
}
