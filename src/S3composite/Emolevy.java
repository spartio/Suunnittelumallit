
package S3composite;

import java.util.ArrayList;
import java.util.List;

/**
 * @author spart
 */
public class Emolevy implements Osa {
    private final double OMAHINTA = 4.3;
    private double summa;
    
    List<Osa> emolista = new ArrayList();
    
    @Override
    public double laskeHinta() {
        for (Osa osa : emolista) {
            summa += osa.laskeHinta();
        }
      
        summa+=OMAHINTA;
    
        System.out.println("emolevy: " + summa);
        return summa;
    }


    @Override
    public void lisääOsa(Osa osa, int määrä) {
        for (int i=0; i<määrä; i++) {   
            emolista.add(osa);
        }
    }
}
