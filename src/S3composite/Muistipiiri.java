
package S3composite;

/**
 * @author spart
 */
public class Muistipiiri implements Osa {
    final double HINTA = 1.5;

    @Override
    public double laskeHinta() {
        return HINTA;
    }

    @Override
    public void lisääOsa(Osa o, int määrä) {
        throw new RuntimeException("Muistipiiriin ei voida lisätä muita osia");
    }
}
