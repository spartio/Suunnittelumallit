
package S7state;

/**
 * @author spart
 */
public class Venusaur implements Evoluutio {
    private int HP=30;
    private String nimi="Venusaur";
    private static Venusaur instance=null;

    private Venusaur(){}
    
    public static Venusaur getInstance() {
        if (instance==null)
            instance=new Venusaur();
        return instance;
    }
    
    @Override
    public void ulvo() {
        System.out.println("murrrrr");
    }

    @Override
    public void hyökkää() {
        System.out.println(nimi + ", ruoskaise!");
    }

    @Override
    public void kehity(Ash a) {
        throw new RuntimeException("Venusaur ei voi kehittyä");
    }
    
    @Override
    public void printHP() {
        System.out.println("HP:" + HP);
    }

    @Override
    public void printNimi() {
        System.out.println(nimi);
    }
}
