
package S7state;

/**
 * @author spart
 */
public class Ivysaur implements Evoluutio {
    private int HP=15;
    private String nimi="Ivysaur";
    private static Ivysaur instance=null;

    private Ivysaur(){}
    
    public static Ivysaur getInstance() {
        if (instance==null)
            instance=new Ivysaur();
        return instance;
    }
    
    @Override
    public void ulvo() {
        System.out.println("mur");
    }

    @Override
    public void hyökkää() {
        System.out.println(nimi + ", heitä lehtiä!");
    }

    @Override
    public void kehity(Ash a) {
        a.kehity(Venusaur.getInstance());
    }

    @Override
    public void printHP() {
        System.out.println("HP:" + HP);
    }

    @Override
    public void printNimi() {
        System.out.println(nimi);
    }
    
    
}
