
package S7state;

/**
 * @author spart
 */
public class Ash {
    private Evoluutio evo;
    
    public Ash() {
        evo = Bulbasaur.getInstance();
    }
    
    public void ulvo() {
        evo.ulvo();
    }

    public void kehity(Evoluutio e) {
        evo=e;
    }
    
    public void hyökkää() {
        evo.hyökkää();
    }
    
    public void kehitä() {
        evo.kehity(this);
    }
    
    public void printHP() {
        evo.printHP();
    }
    public void printNimi() {
        evo.printNimi();
    }
}
