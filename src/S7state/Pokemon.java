
package S7state;

/**
 * @author spart
 */
public class Pokemon {
    public static void main(String[] args) {
        Ash ash = new Ash();
        
        ash.printNimi();
        ash.ulvo();
        ash.hyökkää();
        ash.printHP();
        System.out.println("\n");
        
        ash.kehitä();
        ash.printNimi();
        ash.ulvo();
        ash.hyökkää();
        ash.printHP();
        System.out.println("\n");
        
        ash.kehitä();
        ash.printNimi();
        ash.ulvo();
        ash.hyökkää();
        ash.printHP();
        
        ash.kehitä();
    }
}
