
package S7state;

/**
 * @author spart
 */
public class Bulbasaur implements Evoluutio {
    private static Bulbasaur instance=null;
    private String nimi="Bulbasaur";
    private int HP=10;

    private Bulbasaur(){}
    
    public static Bulbasaur getInstance() {
        if (instance==null)
            instance=new Bulbasaur();
        return instance;
    }
    
    @Override
    public void ulvo() {
        System.out.println("wuh");
    }
 
    @Override
    public void kehity(Ash a) {
       a.kehity(Ivysaur.getInstance());
    }
    
    @Override
    public void hyökkää() {
        System.out.println(nimi + ", taklaa!");
    }
    
    @Override
    public void printHP() {
        System.out.println("HP:" + HP);
    }
    
    @Override
    public void printNimi() {
        System.out.println(nimi);
    }
}
