package S7state;

/**
 *
 * @author spart
 */
public interface Evoluutio {
    public void ulvo();
    public void kehity(Ash a);
    public void hyökkää();
    public void printHP();
    public void printNimi();
}
