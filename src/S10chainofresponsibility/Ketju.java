
package S10chainofresponsibility;

import java.util.Scanner;

/**
 * @author spart
 */
public class Ketju {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double korotusprosentti;

        Lähiesimies lähiesimies = new Lähiesimies();
        Päällikkö päällikkö = new Päällikkö();
        Toimitusjohtaja tj = new Toimitusjohtaja();
        
        lähiesimies.setSeuraaja(päällikkö);
        päällikkö.setSeuraaja(tj);
        
        System.out.println("anna korotusprosentti");
        korotusprosentti = scanner.nextDouble();
        
        lähiesimies.processRequest(new Korotuspyyntö(korotusprosentti));
        
    }
}
