
package S10chainofresponsibility;

/**
 * @author spart
 */
public class Lähiesimies extends Korotusvaltuudet {
    private String role = "Lähiesimies";
    private final double allowable = 2;
    
    @Override
    protected double getAllowable() {
        return allowable;
    }

    @Override
    protected String getRole() {
        return role;
    }
}
