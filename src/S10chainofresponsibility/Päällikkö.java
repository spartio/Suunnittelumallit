
package S10chainofresponsibility;

/**
 * @author spart
 */
public class Päällikkö extends Korotusvaltuudet {
    private String role = "Päällikkö";
    private final double allowable = 5;
    
    @Override
    protected double getAllowable() {
        return allowable;
    }

    @Override
    protected String getRole() {
        return role;
    }
}
