
package S10chainofresponsibility;

/**
 * @author spart
 */
public class Toimitusjohtaja extends Korotusvaltuudet {
    private String role = "Toimitusjohtaja";
    private final double allowable=100;
    
    @Override
    protected double getAllowable() {
        return allowable;
    }

    @Override
    protected String getRole() {
        return role;
    }
}
