
package S10chainofresponsibility;

/**
 * @author spart
 */
public class Korotuspyyntö {
    private double amount;

    public Korotuspyyntö(double amount) {
        this.amount = amount;
    }
        
    public double getAmount() {
        return amount;
    }
}
