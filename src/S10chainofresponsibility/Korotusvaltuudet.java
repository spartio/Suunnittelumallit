package S10chainofresponsibility;

/**
 *
 * @author spart
 */
public abstract class Korotusvaltuudet {
    private Korotusvaltuudet seuraaja;
    
    public void setSeuraaja(Korotusvaltuudet e) {
        seuraaja=e;
    }
    
    public void processRequest(Korotuspyyntö pyyntö){
        if (pyyntö.getAmount() < this.getAllowable()) {
            System.out.println(this.getRole() + " hyväksyy " + pyyntö.getAmount() + "% palkan korotuksen.");
        } else if (seuraaja != null) {
            seuraaja.processRequest(pyyntö);
        } else {
            throw new RuntimeException("Nyt on kyllä liian kunnianhimoinen palkankorotus pyyntö");
        }
    }

    abstract protected double getAllowable();
    abstract protected String getRole();
}
