package S15adapter;

/**
 *
 * @author spart
 */
public interface AdvancedMediaPlayer {	
   public void playVlc(String fileName);
   public void playMp4(String fileName);
}
