package S15adapter;

/**
 *
 * @author spart
 */
public interface MediaPlayer {
   public void play(String audioType, String fileName);
}
