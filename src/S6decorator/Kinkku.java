
package S6decorator;

/**
 * @author spart
 */
public class Kinkku extends Täyte{
    private double omaHinta=3.8;
    private String description="pirkan palvikinkku";
    
    public Kinkku(Pizza taikina) {
        super(taikina);
    }

    public double getHinta() {
        return super.getHinta()+omaHinta;
    }

    public String getDescription() {
        return description;
    }
}
