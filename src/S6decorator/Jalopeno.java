
package S6decorator;

/**
 * @author spart
 */
public class Jalopeno extends Täyte {
    private double omaHinta = 7.2;
    private String description="täräyttävä jalopeno";
    public Jalopeno(Pizza taikina) {
        super(taikina);
    }

    public double getHinta() {
        return super.getHinta()+omaHinta;
    }

    public String getDescription() {
        return super.getDescription() + description + "\n\t";
    }
}
