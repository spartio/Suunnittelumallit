package S6decorator;

/**
 *
 * @author spart
 */
public interface Pizza {
    public double getHinta();
    public String getDescription();
}
