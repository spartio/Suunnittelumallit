
package S6decorator;

/**
 * @author spart
 */
public class Valkosipuli extends Täyte {
    private double omaHinta=2.7;
    private String description="valkosipuli";
    
    public Valkosipuli(Pizza taikina) {
        super(taikina);
    }

    public double getHinta() {
        return super.getHinta()+omaHinta;
    }

    public String getDescription() {
        return super.getDescription() + description + "\n\t";
    }
}
