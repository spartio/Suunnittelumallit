
package S6decorator;

/**
 * ananas=1.1
 * tomaatti=1.4
 * taikina=2.4
 * kinkku=3.8
 * valkosipuli=2.7
 * jalopeno=7.2
 */
public class Pizzeria {
    public static void main(String[] args) {
        Pizza margarita = 
            new Tomaattipohja(
                new Ananas(
                    new Taikina()));
        System.out.println("Margarita: " + margarita.getHinta() + "€");
        System.out.println(margarita.getDescription() + "\n");
        
        Pizza fruttidimare =
            new Tomaattipohja(
                new Valkosipuli(
                    new Taikina()));
        System.out.println("Frutti di Mare: " + fruttidimare.getHinta() + "€");
        System.out.println(fruttidimare.getDescription()+ "\n");
    
        Pizza quattrostagione = 
            new Valkosipuli(
                new Jalopeno(
                    new Taikina()));
        System.out.println("Quattro Stagione: " + quattrostagione.getHinta() + "€");
        System.out.println(quattrostagione.getDescription());
    }
}
