
package S6decorator;

/**
 * @author spart
 */
public abstract class Täyte implements Pizza {
    protected Pizza taikina;
    
    public Täyte (Pizza taikina) {
        this.taikina = taikina;
    }
    
    public double getHinta() {
        return taikina.getHinta();
    }
    
    public String getDescription() {
        return taikina.getDescription();
    }
}
