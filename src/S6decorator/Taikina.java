
package S6decorator;

/**
 * @author spart
 */
public class Taikina implements Pizza {
    private double hinta=2.4;
    private String description="ruistaikina";

    public double getHinta() {
        return hinta;
    }

    public String getDescription() {
        return "\t" + description + "\n\t";
    }
}
