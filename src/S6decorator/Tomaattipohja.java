
package S6decorator;

/**
 * @author spart
 */
public class Tomaattipohja extends Täyte {
    private double omaHinta=1.4;
    private String description="tomaattikastike";
    
    public Tomaattipohja(Pizza taikina) {
        super(taikina);
    }

    @Override
    public double getHinta() {
        return super.getHinta() + omaHinta;
    }

    @Override
    public String getDescription() {
        return super.getDescription() + description + "\t";
    }
}
