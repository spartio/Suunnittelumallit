
package S6decorator;

/**
 * @author spart
 */
public class Ananas extends Täyte {
    private double omaHinta=1.1;
    private String description="herkullinen ananas";
    
    public Ananas(Pizza taikina) {
        super(taikina);
    }

    public double getHinta() {
        return super.getHinta() + omaHinta;
    }

    public String getDescription() {
        return super.getDescription() + description + "\n\t" ;
    }
}
