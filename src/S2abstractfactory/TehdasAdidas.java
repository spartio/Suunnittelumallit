
package S2abstractfactory;

/**
 * @author spart
 */
public class TehdasAdidas implements AbstractTehdas {
    @Override
    public AbstractKengät luoKengät() {
        return new KengätA();
    }

    @Override
    public AbstractLippis luoLippis() {
        return new LippisA();
    }
    
    @Override
    public AbstractPaita luoPaita() {
        return new PaitaA();
    }

}
