
package S2abstractfactory;

/**
 * @author spart
 */
public class TehdasBoss implements AbstractTehdas {
    @Override
    public AbstractKengät luoKengät() {
        return new KengätB();
    }

    @Override
    public AbstractLippis luoLippis() {
        return new LippisB();
    }
    
    @Override
    public AbstractPaita luoPaita() {
        return new PaitaB();
    }

}
