
package S2abstractfactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author spart
 */
public class Jasper {
     
    public static void main(String[] args) {
        Class c = null;
        AbstractTehdas tehdas = null;
        
        Properties prop = new Properties();
        
        try {
            prop.load(Jasper.class.getResourceAsStream("/tehdas.properties"));
        } catch (IOException e) {e.printStackTrace();}
        
        
        try {
            c = Class.forName(prop.getProperty("tehdas"));
            tehdas = (AbstractTehdas)c.newInstance();
        } catch (Exception e){e.printStackTrace();}
        
        
        AbstractKengät kengät = tehdas.luoKengät();
        AbstractLippis lippis = tehdas.luoLippis();
        AbstractPaita paita = tehdas.luoPaita();
        
        String tarina = "Oon Jasper. Mul on: ";
        System.out.println(tarina);
        System.out.println(kengät.toString());
        System.out.println(lippis.toString());
        System.out.println(paita.toString());
    }
}
