package S2abstractfactory;

/**
 *
 * @author spart
 */
public interface AbstractTehdas {
    public AbstractKengät luoKengät();
    public AbstractLippis luoLippis();
    public AbstractPaita luoPaita();
}
