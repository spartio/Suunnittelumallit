
package S11memento;

/**
 * @author spart
 */
import java.util.Random;
//CARETAKER
public class Arvaaja implements Runnable {
    Random rand = new Random();
    private Object obj;
    Arvuuttaja arvuuttaja = new Arvuuttaja();
    boolean arvaa=true;
    private String nimi;

    public Arvaaja(String nimi) {
        this.nimi = nimi;
    }
    
    public synchronized void arvaaLukua(Arvuuttaja arvuuttaja) {
        int luku = rand.nextInt(100) + 1;
        
        if (arvuuttaja.tarkistaLuku(obj, luku, nimi))
            arvaa=false;
    }
    
    public void tallennaMemento(Object obj) {
        this.obj=obj;
    }

    @Override
    public void run() {
        while (arvaa) {
            arvaaLukua(arvuuttaja);
        }
    }

    public String getNimi() {
        return nimi;
    }
}
