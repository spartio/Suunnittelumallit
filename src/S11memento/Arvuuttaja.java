
package S11memento;

/**
 * @author spart
 */
import java.util.Random;
//ORIGINATOR
public class Arvuuttaja {
    Random random = new Random();
    private int oikeaLuku;
    private int maxLuku=100;
    
    public void liityPeliin(Arvaaja arvaaja) {
        oikeaLuku = random.nextInt(maxLuku) + 1;
        
        arvaaja.tallennaMemento(new Memento(oikeaLuku));
    }
    
    public synchronized boolean tarkistaLuku(Object obj, int i, String nimi) {
        Memento memento = (Memento) obj;
        System.out.println("\n");
        System.out.println(nimi);
        System.out.println(i);
        System.out.println(memento.arvattavaluku);
        if (memento.arvattavaluku==i) {
            System.out.println("oikein");
            return true;
        } else {
            System.out.println("väärin");
            return false;
        } 
    }

    private class Memento {
        private int arvattavaluku;

        public Memento(int arv) {
            this.arvattavaluku=arv;
        }
    }
}
