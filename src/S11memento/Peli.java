
package S11memento;

/**
 * @author spart
 */
public class Peli {
    public static void main(String[] args) {
        Arvuuttaja arvuuttaja = new Arvuuttaja();
        Arvaaja arv1 = new Arvaaja("arvaaja 1");
        Arvaaja arv2 = new Arvaaja("arvaaja 2");
        Arvaaja arv3 = new Arvaaja("arvaaja 3");
        
        arvuuttaja.liityPeliin(arv1);
        arvuuttaja.liityPeliin(arv2);
        arvuuttaja.liityPeliin(arv3);
        
        new Thread(arv1).start();
        new Thread(arv2).start();   
        new Thread(arv3).start();
    }

}
