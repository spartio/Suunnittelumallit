
package S4observer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author spart
 */
public class KelloAjastin extends Observable implements Runnable {
    private int tunti, minuutti, sekuntti;
    
    void tick() {
        Calendar cal = Calendar.getInstance();
        
        tunti = cal.get(Calendar.HOUR_OF_DAY);
        minuutti = cal.get(Calendar.MINUTE);
        sekuntti = cal.get(Calendar.SECOND);
    }
    
    public int getTunti() {
        return tunti;
    }

    public int getMinuutti() {
        return minuutti;
    }

    public int getSekuntti(){
        return sekuntti;
    }

    @Override
    public void run() {
        while (true) {
            tick();
            setChanged();
            notifyObservers();
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
