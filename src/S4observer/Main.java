
package S4observer;

import java.util.Observer;

/**
 * @author spart
 */

public class Main {
    public static void main(String[] args) {
        KelloAjastin ajastin = new KelloAjastin();
        Observer digitaalikello = new DigitaaliKello(ajastin);
        
        new Thread(ajastin).start();
    }
}
