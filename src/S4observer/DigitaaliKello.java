
package S4observer;

import java.util.Observable;
import java.util.Observer;

/**
 * @author spart
 */
public class DigitaaliKello implements Observer {
    private int tunti, minuutti, sekuntti;
    private KelloAjastin ajastin;
    
    public DigitaaliKello (KelloAjastin ka) {
        ajastin = ka;
        ajastin.addObserver(this);
    }
    
    @Override
    public void update(Observable o, Object arg) {
        tunti = ajastin.getTunti();
        minuutti = ajastin.getMinuutti();
        sekuntti = ajastin.getSekuntti();
        
        System.out.println(tunti+":"+minuutti+":"+sekuntti);
    }
    

}
